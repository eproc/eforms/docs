---
title: Code list on FSR
date: 17 May 2024
identifier: cdl-001-fsr
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Information

* **Name:** Foreign Subsidy Measure Conclusion
* **IRI:** foreign-subsidy-measure-conclusion
* **ePO:** epo:ForeignSubsidyMeasure
* **Dataset type:** Authority table
* **Author:** European Commission
* **Description:** On 12 July 2023, the Foreign Subsidies Regulation (FSR) (Regulation (EU) 2022/2560) entered into force. The FSR introduces a new set of rules that enables the Commission to address distortions caused by foreign subsidies. This allows the EU to ensure a level of playing field for all tenderers operating in the Single Market, while remaining open to trade and investment. This code list is about the situation of Measures applied under the Foreign Subsidies Regulation (EU) 2022/2560. Economic operators bidding in large public procurement procedures must submit a notification or declaration pursuant to Article 28(1) FSR with their tender, which is assessed by the Commission. The economic operators either provide a notification or a declaration under the FSR, depending on the amount of foreign financial contribution received. The contract award notice must acknowledge the Commission’s assessment, based on the situations described in the Table below. 

# Table

| Code       | Label                                      | Definition                                                   |
| ---------- | ------------------------------------------ | ------------------------------------------------------------ |
| no-obj     | Notification submitted, decision with no objection           | The Commission conducted its in-depth investigation and concluded that there is no distortion, the Buyer can award the contract to the tenderer that was under in-depth investigation, the FSR procedure is closed. |
| proh       | Notification submitted, decision prohibiting the award       | The Commission conducted its in-depth investigation and concluded that there are distortive foreign subsidies. Therefore, it issued a decision to the Buyer prohibiting the award to the tenderer that was under in-depth investigation. |
| irregul    | Notification submitted, decision on irregular tender or request to participate | The Commission conducted a preliminary review and found that the notification was incomplete, and requested more information from the economic operator who did not comply. The Commission declared the tender or request to participate irregular and requests the Buyer to reject the tender or request to participate. |
| commit     | Notification submitted, decision with commitments            | The Commission conducted its in-depth investigation and concluded that there are distortive foreign subsidies, but the tenderer provided commitments that remedy the distortion. The Buyer can award the contract to the tenderer that was under in-depth investigation. |
| admin-clos | Notification submitted, administrative closure of FSR preliminary review procedure | The Commission conducted its preliminary review procedure and did not issue a decision before the end of the time limit prescribed by FSR. The FSR procedure ended, and the Buyer can award the contract to the tenderer that submitted the notification. |
| stand      | Declaration submitted, standard award procedure              | A tenderer submitted a declaration to the Commission, but the Commission did not open an in-depth investigation. Therefore, the Buyer can award the contract to the tenderer that submitted that declaration. |
| meat       | Declaration submitted, standard MEAT award                   | The Commission has started an in-depth investigation on the tenderer. However, this tenderer did not submit the most economically advantageous tender (MEAT). There was another tenderer who submitted a declaration and MEAT. The Commission did not open an in-depth investigation against that tenderer. Therefore, the Buyer can award the contract to that tenderer with the MEAT. |

Table: Code list 
