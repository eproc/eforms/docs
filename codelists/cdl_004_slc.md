---
title: Code list on Selection Criterion
date: 17 May 2024
identifier: cdl-004-slc
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Information

* **Name:** Selection Criterion
* **IRI:** selection-criterion
* **ePO:** epo:SelectionCriterion 
* **Dataset type:** Taxonomy
* **Author:** European Commission
* **Description:** Selection criteria in procurement are the requirements for participation in the procurement process, focusing on tenderers suitability, economic and financial standing, and technical and professional ability, and ensuring tender relevance and proportionality to the contract.

# Table

| Code                     | Label                         | Definition                                                   |
| ------------------------ | ----------------------------- | ------------------------------------------------------------ |
| **slc-abil** | **Technical and professional ability** | **Selection criteria based on the technical and qualification capacities of the economic operator.** |
| slc-abil-facil-res | Study, technical and research facilities | The economic operator can use study, technical and research facilities. |
| slc-abil-facil-tools | Tools, plant, or technical equipment | The economic operator can use tools, plant, or technical equipment. |
| slc-abil-mgmt-env | Environmental management measures | The economic operator can apply environmental management measures. |
| slc-abil-mgmt-supply | Supply chain management | The economic operator can apply supply chain management and tracking systems. |
| slc-abil-mgmt-qual | Measures for ensuring quality | The economic operator can put measures in place for ensuring quality. |
| slc-abil-qual-inst | Certificates by quality control institutes  | The economic operator can provide the required certificates drawn up by official quality control institutes or agencies of recognised competence attesting the conformity of products clearly identified by references to the technical specifications or standards. |
| slc-abil-qual-smp-w-autent | Samples, descriptions, or photographs with certification of authenticity for supply contracts | The economic operator can supply the required samples, descriptions, or photographs of the products to be supplied and to provide certifications of authenticity. |
| slc-abil-qual-smp-wo-autent | Samples, descriptions, or photographs without certification of authenticity | The economic operator can supply the required samples, descriptions, or photographs of the products to be supplied, which do not need to be accompanied by certifications of authenticity. |
| slc-abil-ref-services | References on specified services  | References for the specified services, during the specified period, performed by the economic operator. Buyers may require up to three years’ experience and allow experience dating from more than three years. |
| slc-abil-ref-supply | References on specified deliveries | References for the specified deliveries, during the specified period, performed by the economic operator. Buyers may require up to three years’ experience and allow experience dating from more than three years for supply contracts. |
| slc-abil-ref-work | References on specified works | References for the specified works, during the specified period, performed by the economic operator. Buyers may require up to five years’ experience and allow experience dating for more than five years. |
| slc-abil-staff-qual | Relevant educational and professional qualifications | The economic operator has the relevant educational and professional qualifications. |
| slc-abil-staff-tech-ctrl | Technicians or technical bodies for quality control | The economic operator employs technicians or technical bodies, especially those responsible for quality control. |
| slc-abil-staff-tech-work | Technicians or technical bodies to carry out the work | The economic operator employs technicians or technical bodies to carry out the work, in the case of works contracts. |
| slc-abil-staff-yrly-avg-mp | Average yearly manpower | The average yearly manpower of the economic operator for the last three years. |
| slc-abil-staff-yrly-no-mgmt | Number of managerial staff | The number of managerial staff of the economic operator for the last three years. |
| slc-abil-subc | Subcontracting proportion | The economic operator intends possibly to subcontract a proportion of the contract. |
| **slc-sche** | **Quality assurance schemes and environmental management standards** | **Selection criteria based on quality assurance schemes and environmental management standards.**  |
| slc-sche-env-cert-indep | Certificates by independent bodies about environmental management systems or standards | The economic operator can produce certificates drawn up by independent bodies attesting that the economic operator complies with the required environmental management systems or standards. |
| slc-sche-qu-cert-indep | Certificates by independent bodies about quality assurance standards | The economic operator can produce certificates drawn up by independent bodies attesting that the economic operator complies with the required quality assurance standards, including accessibility for disabled persons. |
| **slc-sec** | **Ability to secure information, processes, and deliveries** | **Selection criteria based on the ability to secure information, processes, and certification on deliveries by the economic operator.** |
| slc-sec-inf | Security of information | The economic operator commits to appropriately safeguard the confidentiality of all classified information in the contractor’s possession or coming to their notice throughout the duration of the contract and after termination or conclusion of the contract, in accordance with the relevant laws, Regulations and administrative provisions. |
| slc-sec-proc | Security to process, store and transmit classified information | The economic operator is able, in the case of contracts involving, entailing and/or containing classified information, to provide evidence of the ability to process, store and transmit such information at the level of protection required by the buyer. |
| slc-sec-supply | Security of supply | The economic operator can provide certification showing the ability to honour obligations regarding the export, transfer, and transit of goods during the contract, and to comply with the related requirements of the buyer. |
| **slc-stand** | **Economic and financial standing** | **Selection criteria based on the level of financial risk status of the economic operator.** |
| slc-stand-ins | Professional risk indemnity insurance | A professional risk indemnity insurance is required for the performance on this contract. |
| slc-stand-other | Other economic or financial requirements | Other economic or financial requirements of the economic operator. |
| slc-stand-ratio | Financial ratio | The actual values for the required financial ratios of the economic operator. |
| slc-stand-to-avg | Average yearly turnover | The average yearly turnover for the required number of financial years of the economic operator. |
| slc-stand-to-gen | General yearly turnover | The general yearly turnover for the required number of financial years of the economic operator. |
| slc-stand-to-spec | Specific yearly turnover | A specific yearly turnover in the business area covered by the procurement concerned for the required number of financial years is required of the economic operator. |
| slc-stand-to-spec-avg | Specific average yearly turnover | A specific average yearly turnover in the business area covered by the contract for the required number of years is required of the economic operator. |
| **slc-suit** | **Suitability** | **Selection criteria based on ability of the economic operator to attend professional exercise.** |
| slc-suit-auth-mbrshp | Authorisation or membership of a particular organisation needed for service contracts | The economic operator has the necessary membership or authorisation of a particular organisation, to be able to perform the service in the country of its establishment. |
| slc-suit-reg-prof | Enrolment in a relevant professional register | The economic operator is enrolled in the relevant professional registers kept in the Member State of its establishment as described in the relevant legislation. |
| slc-suit-reg-trade | Enrolment in a trade register | The economic operator is enrolled in trade registers kept in the Member State of its establishment as described in the relevant legislation. |


Table: Code list 
