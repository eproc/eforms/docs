---
title: Code list on Documents used in Public Procurement
date: 17 May 2024
identifier: cdl-002-dpp
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Information

* **Name:** Document used in Public Procurement
* **IRI:** document-used-in-public-procurement
* **ePO:** epo:Document 
* **Dataset type:** Authority table
* **Author:** European Commission
* **Description:** This code list describes documents used in Public Procurement. A set of interrelated business information representing the business facts and associated metadata. The information may be conveyed in any language, medium or form, including textual, numerical, graphic, cartographic, audio-visual forms, etc.

# Table

| Code                     | Label                            | Definition                                                   |
| ------------------------ | -------------------------------- | ------------------------------------------------------------ |
| epo-sub-espd             | European Single Procurement Document (ESPD) | A self-declaration used as preliminary evidence in replacement of certificates issued by public authorities or third parties confirming that the relevant economic operator fulfils the conditions. It is sent at the time of submission of requests to participate or of tenders and must be accepted by the Buyer. |
| epo-notice               | Notice                                      | A document published by the Buyer about business opportunities and results. |
| epo-procurement-document | Procurement Document                        | A document produced or referred to by the Buyer to describe or determine elements of the Procurement Procedure. Additional information: Procurement Documents are to be accessible at the date of publication of the Competition Notice. Examples of Procurement Documents are Technical Specifications, Descriptive Documents, draft Contract, the European Single Procurement Document (ESPD).The Commission conducted its in-depth investigation and concluded that there are distortive foreign subsidies, but the tenderer provided commitments that remedy the distortion. The Buyer can award the contract to the tenderer that was under in-depth investigation. |

Table: Code list 
