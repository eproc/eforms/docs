---
title: Code list on Exclusion Grounds
date: 17 May 2024
identifier: cdl-003-exg
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Information

* **Name:** Exclusion Ground
* **IRI:** exclusion-ground
* **ePO:** epo:ExclusionGround 
* **Dataset type:** Taxonomy
* **Author:** European Commission
* **Description:** Exclusion grounds in public procurement refer to the criteria that prevent certain entities from participating in public contracts due to criminal offenses, tax breaches, or other specified misconduct, aiming to ensure fairness and integrity in public procurement processes.

# Table

| Code                     | Label                         | Definition                                                   |
| ------------------------ | ----------------------------- | ------------------------------------------------------------ |
| **exg-crim**             | **Grounds relating to criminal convictions**                 | **Exclusion grounds that are related to criminal convictions.** |
| exg-crim-corr     | Corruption                                                   | The economic operator itself or any person who is a member of its administrative, management or supervisory body or powers of representation, decision or control therein has been the subject of a conviction by final judgement for corruption, by a conviction rendered at the most five years ago or in which an exclusion period set out directly in the conviction continues to be applicable. Corruption, as defined in the relevant legislation. |
| exg-crim-laund       | Money laundering or terrorist financing                      | The economic operator itself or any person who is a member of its administrative, management or supervisory body or powers of representation, decision or control therein has been the subject of a conviction by final judgement for money laundering or terrorist financing, by a conviction rendered at the most five years ago or in which an exclusion period set out directly in the conviction continues to be applicable. Money laundering or terrorist financing, as defined in the relevant legislation. |
| exg-crim-fraud       | Fraud                                                        | The economic operator itself or any person who is a member of its administrative, management or supervisory body or powers of representation, decision or control therein has been the subject of a conviction by final judgement for fraud, by a conviction rendered at the most five years ago or in which an exclusion period set out directly in the conviction continues to be applicable. Fraud within the meaning of the relevant legislation. |
| exg-crim-traffick    | Child labour and including other forms of trafficking in human beings | The economic operator itself or any person who is a member of its administrative, management or supervisory body or powers of representation, decision or control therein has been the subject of a conviction by final judgement for child labour including other forms of trafficking in human beings, by a conviction rendered at the most five years ago or in which an exclusion period set out directly in the conviction continues to be applicable. Child labour and other forms of trafficking in human beings, as defined in the relevant legislation. |
| exg-crim-part        | Participation in a criminal organisation                     | The economic operator itself or any person who is a member of its administrative, management or supervisory body or powers of representation, decision or control therein has been the subject of a conviction by final judgement for participation in a criminal organisation, by a conviction rendered at the most five years ago or in which an exclusion period set out directly in the conviction continues to be applicable. Participation in a criminal organisation, as defined in the relevant legislation. |
| exg-crim-terror      | Terrorist offences or offences linked to terrorist activities | The economic operator itself or any person who is a member of its administrative, management or supervisory body or powers of representation, decision or control therein has been the subject of a conviction by final judgement for terrorist offences or offences linked to terrorist activities, by a conviction rendered at the most five years ago or in which an exclusion period set out directly in the conviction continues to be applicable. Terrorist offences or offences linked to terrorist activities, as defined in the relevant legislation. |
| **exg-mis**            | **Grounds relating to insolvency, conflicts of interests or professional misconduct** | **Exclusion grounds that are related to insolvency, conflicts of interests or professional misconduct.** |
| exg-mis-bre-env-law  | Breaching of obligations in the fields of environmental law  | The economic operator has to its knowledge, breached its obligations in the field of environmental law. |
| exg-mis-bre-lab-law  | Breaching of obligations in the fields of labour law         | The economic operator has to its knowledge, breached its obligations in the field of labour law. |
| exg-mis-bre-soc-law  | Breaching of obligations in the fields of social law         | The economic operator has to its knowledge, breached its obligations in the field of social law. |
| exg-mis-distortion   | Agreements with other economic operators aimed at distorting competition | The economic operator has entered into agreements with other economic operators aimed at distorting competition. |
| exg-mis-misconduct   | Grave professional misconduct                                | The economic operator is guilty of grave professional misconduct. Where applicable, see definitions in national law, the relevant notice, or the procurement documents |
| exg-mis-misrepresent | Misrepresentation, withheld information, unable to provide required documents and obtained confidential information of this procedure | The economic operator has been guilty of serious misrepresentation in supplying the information required for the verification of the absence of grounds for exclusion or the fulfilment of the selection criteria, it has withheld such information, it has not been able, without delay, to submit the supporting documents required by a buyer, or it has undertaken to unduly influence the decision making process of the buyer, to obtain confidential information that may confer upon it undue advantages in the procurement procedure or to negligently provide misleading information that may have a material influence on decisions concerning exclusion, selection or award. |
| exg-mis-off-cond     | Offence concerning its professional conduct in the domain of defence procurement | The economic operator has been convicted by a judgment which has the force of res judicata in accordance with the legal provisions of the country in which it is established or in the country of the buyer of any offence concerning its professional conduct, such as, for example, infringement of existing legislation on the export of defence and/or security equipment. |
| exg-mis-partic-confl | Conflict of interest due to its participation in the procurement procedure | The economic operator is aware of any conflict of interest due to its participation in the procurement procedure. |
| exg-mis-prep-confl   | Direct or indirect involvement in the preparation of this procurement procedure | The economic operator or an undertaking related to it provided advice to the buyer or otherwise has been involved in the preparation of the procurement procedure. |
| exg-mis-sanction     | Early termination, damages, or other comparable sanctions    | A prior contract with a buyer or a prior contract executed by the economic operator was terminated early, or damages or other comparable sanctions were imposed in connection with that prior contract. |
| exg-mis-unrel-sec    | Lack of reliability to exclude risks to the security of the country | The economic operator has been found based on any means of evidence, including protected data sources, not to possess the reliability necessary to exclude risks to the security of the country. |
| **exg-natl**             | **Purely national exclusion grounds**                        | **Other exclusion grounds**                                  |
| exg-natl-bre-nat-law | Breaching of obligations set under purely national exclusion grounds | The economic operator has breached its obligations set under purely national grounds of exclusion. |
| **exg-pmt**              | **Grounds relating to the payment of taxes or social security contributions** | **Exclusion grounds that are related to the payment of taxes or social security contributions.** |
| exg-pmt-bre-ssc      | Breaching obligation relating to payment of social security contributions | The economic operator has breached its obligations relating to the payment social security contributions, either in the country in which it is established or in the country of the buyer if other than the country of establishment. |
| exg-pmt-bre-tax      | Breaching obligation relating to payment of taxes            | The economic operator has breached its obligations relating to the payment of taxes, either in the country in which it is established or in the country of the buyer if other than the country of establishment. |
| **exg-sitn**           | **Grounds relating to the situation of the economic operator** | **Exclusion grounds that are related to the situation of the economic operator.** |
| exg-sitn-as-susp     | Business activities are suspended                            | The business activities of the economic operator are suspended. |
| exg-sitn-bankr       | Bankruptcy                                                   | The economic operator is bankrupt.                           |
| exg-sitn-cred-arran  | Arrangement with creditors                                   | The economic operator is in arrangement with creditors.      |
| exg-sitn-insolvency  | Insolvency                                                   | The economic operator is the subject of insolvency or winding-up of its business. |
| exg-sitn-liq-admin   | Assets being administered by liquidator                      | The assets of the economic operator are being administered by a liquidator or by the court. |
| exg-sitn-other       | Analogous situation like bankruptcy, insolvency or arrangement with creditors under national law | The economic operator is in any analogous situation to bankruptcy, insolvency or arrangement with creditors arising from a similar procedure under national laws and regulations. |

Table: Code list 
