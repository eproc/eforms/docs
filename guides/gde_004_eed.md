---
title: eForms Guide on Energy Efficiency Directive
date: 16 October 2024
identifier: gde-004-eed
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Objective

On 20 September 2023, the revised [Energy Efficiency Directive](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=OJ%3AJOL_2023_231_R_0001&qid=1695186598766) (EED) EU/2023/1791 entered into force.

The 2023 revised Directive raises the [EU energy efficiency target](https://energy.ec.europa.eu/topics/energy-efficiency/energy-efficiency-targets-directive-and-rules/energy-efficiency-targets_en), making it binding for EU countries to collectively ensure an additional 11.7% reduction in energy consumption by 2030, compared to the 2020 reference scenario projections. As a result, overall EU energy consumption by 2030 should not exceed 992.5 million tonnes of oil equivalent (Mtoe) for primary energy and 763 Mtoe for final energy.

# Application

The reporting obligation under the EED recast is stated under Article 7 of the EED recast, requiring Member States to ensure that buyers, when concluding public contracts and concessions that meet or exceed the thresholds laid down in Directives 2014/23/EU, 2014/24/EU and 2014/25/EU, procure only products, services, buildings and also works with high energy-efficiency performance. In addition, the scope of this obligation is extended compared to the EED 2018 to all buyers, and thus to all levels of public administration.

The services, products, buildings and works purchased by contracting authorities and contracting entities must achieve a high energy-efficiency performance. The energy efficiency requirements to be observed in this context are laid down in more detail in Annex IV of the EED recast and cover:

* products covered by an Energy Labelling delegated act;
* products not covered by an Energy Labelling delegated act but covered by an implementing measure under the Ecodesign Directive 2009/12/EC;
* products and services covered by EU green public procurement criteria or available equivalent national criteria;
* tyres, the highest fuel energy efficiency class, as defined by Regulation (EU) 2020/740;
* buildings. Although buildings are not part of a procurement procedure, transparency in the procurement process as regards the impact on energy efficiency for the purchase or new rental agreements for buildings in accordance with Article 7 of Directive (EU) 2023/1791 needs to be ensured.

If products are covered by an Energy Labelling delegated act or by an implementing measure under the Directive 2009/125/EC and EU GPP criteria, the most ambitious energy-efficiency requirement is to be considered. If the EU GPP criteria are more ambitious, Member States shall consider and make best efforts, as provided in Article 7(5) and Annex IV point (c) of the Directive (EU) 2023/1791, to use these EU GPP. 

The general obligation in Article 7(1) of Directive (EU) 2023/1791 does not apply if it is not technically feasible. The condition of feasibility applies to all public procurement contracts regardless of which of the Directives on public procurement the contract falls under. It is up to buyers to assess feasibility case by case before launching a procurement procedure and be able to demonstrate that requiring a high energy-efficiency performance in a contract or concession is not technically feasible. If this cannot be proven, contracting authorities or contracting entities must purchase available energy efficient products, services, buildings and works in accordance with the requirements referred to in Annex IV to Directive (EU) 2023/1791.

# Fields

| Id           | Name                                                               | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Type           | Rep.         | Comp.       | Res.  |
| ------------ | ------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- | ------------ | ----------- | ----- |
| ***BG-809*** | *Energy Efficiency Directive (EED)*                                | *Section on Energy Efficiency Directive (EED)*                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | -              | *No*         | *Yes*       | *Yes* |
| BT-810       | EED Applicable                                                     | The Energy Efficiency Directive (EED) (EU) 2023/1791 is applicable for this procurement procedure.                                                                                                                                                                                                                                                                                                                                                                                                                                               | Ind            | No           | Yes         | Yes   |
| **BG-810**   | EED Information                                                    | Information on ~~the~~ energy efficiency. ~~of the products selected in BT-811.~~                                                                                                                                                                                                                                                                                                                                                                                                                                                                | -              | Yes          | ~~No~~*Yes* | Yes   |
| BT-811       | EED ~~Product~~ List                                               | ~~List~~Selection of products, *works and services* covered by the Energy Labelling Directive, by the Ecodesign Directive, by Union green public procurement criteria, by Regulation 2020/740. *For reporting purposes, buildings covered by the obligation in Article 7 EED are also included. As the list has two levels, it is split into (a) the basis like “Eco label” and (b) the actual item like  “Dishwashers".* ~~and buildings, not being rented or purchased for the purposes listed in EED Annex IV point f) (i), (ii) and (iii).~~ | Code           | ~~Yes~~ *No* | Yes         | Yes   |
| BT-812       | Energy Efficiency Label                                            | Energy efficiency labels that are provided for the products (A+++, A++, A+, A to G).                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Code           | No           |             | Yes   |
| BT-813       | Energy *Consumption in kWh/year* ~~Efficiency~~  ~~Numeric Value~~ | Energy ~~efficiency numeric values~~ *consumption in kWh/year* for products ~~and services~~ for which no label exist *as well as for services, works and buildings. Please report the yearly energy consumption*.                                                                                                                                                                                                                                                                                                                               | Num            | No           |             | Yes   |
| BT-814       | Energy *Savings in kWh/year* ~~Efficiency Unit~~                   | *The value of Energy Savings in kWh/year for products, services, works and buildings. Please report the yearly energy savings.* ~~The unit of the energy efficiency number, for example, kWh/year.~~                                                                                                                                                                                                                                                                                                                                             | ~~Code~~ *Num* | No           |             | Yes   |
| BT-815       | Energy Efficiency Quantity                                         | The quantity provided or estimated. ~~for the product or service.~~ *For products please report the number of products. For buildings, please report the useful floor area in m2. For works or services the value will usually be 1.*                                                                                                                                                                                                                                                                                                            | Num            | No           |             | Yes   |

Table: List of fields in competition (Comp.) and result (Res.) notices

# Approach

EED fields are currently optional in the 2nd amendment. 

## For competition notices

The main idea is that the buyer indicates in `Energy Efficiency Directive (EED) (BG-809)` of the competition notice if Energy Efficiency is applicable for the procurement procedure and for what products, buildings, services or works. For this, the buyer indicates first in `EED Applicable (BT-810)` whether the type of services, products, buildings, or works falls under the EED or not. Then the list of products, services, buildings or works relevant for the procurement procedure need to be selected through `EED List (BT-811)`. As the list has two levels, it is split into `(a)` the basis like `Eco label` and `(b)` the actual item like `Dishwashers`.

## For result notices

In the result notice, the buyer should then indicate the actual values for each selected product, services, buildings or works. There are two different possibilities to provide this information.

Option 1: For products with Energy Labels

1. If the selected item under `EED List (BT-811)` is covered by an Energy Labelling Delegated Act, the buyer should indicate the procured label in `Energy Efficiency Label (BT-812)`.
2. It is possible to also provide the energy savings, covered by an Energy Labelling Delegated Act in `Energy Savings in kWh/year (BT-814)`.
3. The quantity of the procured product should be provided in `Energy Efficiency Quantity (BT-815)`.

Option 2: For services, works, buildings and for products without Energy Efficiency Labels Energy Efficiency

1. If the selected item under `EED List (BT-811)` does not have an Energy Efficiency label, the buyer should indicate the consumption in `Energy Consumption in kWh/year (BT-813)` and savings in `Energy Savings in kWh/year (BT-814)`.
2. The quantity of the procured product or the useful floor area of the building should be provided in `Energy Efficiency Quantity (BT-815)`. For products please report the number of products. For works or services would be usually 1.
3. For buildings covered by the reporting obligation in Article 7 EED, the following information should be added in `Additional Information (BT-300)`:

>This is not a procurement procedure but serves to ensure transparency in the procurement process as regards the impact on energy efficiency for the purchase or new rental agreements for buildings in accordance with Article 7 of Directive (EU) 2023/1791.

# Code lists

| Id  | Field                                                                             | Link                                                                                                                                                                                                                    |
| --- | --------------------------------------------------------------------------------- | ------------------------- |
| 1   | `Energy Efficiency Label (BT-812)` will include the values A+++, A++, A+, A to G. |                                                                                                                                                                                                                         |
| 2   | `EED List (BT-811)`                                                               | PDF [List from DG ENER](https://energy-efficient-products.ec.europa.eu/document/download/0b010bf9-f90d-44a0-8426-76811eb82581_en?filename=Summary%20overview%20of%20ED-EL%20measures%20April2024%20v1-%20for%20web.pdf) |

Table: List of code lists used

# Business rules

As the fields on EED are all optional, the following business rules can be for the time being enforced only at national level. It is recommended to implement the rules to ensure consistency of the fields. 

| Id  | Description                                                                                                                                                                                         |
| --- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1   | In competition notices, if it is indicated that the procurement procedure falls under EED through `EED Applicable (BT-810)`, then at least one product should be selected from `EED List (BT-811)`. |
| 2   | In result notices, for each product selected, either `Energy Efficiency Label (BT-812)` or `Energy Consumption in kWh/year (BT-813)` must be provided.                                              |
| 3   | If `Energy Consumption in kWh/year (BT-813)` is provided, then also r `Energy Savings in kWh/year (BT-814)` must be provided.                                                                       |
| 3   | In result notices, for each product or building, `Energy Efficiency Quantity (BT-815)` must be provided.                                                                                            |

Table: List of business rules

# Tailoring

| Id     | Name                                | Tailoring                                                                                                                                                                                                          |
| ------ | ----------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| BG-809 | Energy Efficiency Directive Section | For the time being, the section on EED is optional. Therefore, it is possible to implement it later. If it is implemented, the business rules as indicated in section `Business rules` should also be implemented. |

Table: Possibilities for tailoring

# Analysis

| Id  | Name                                             | Description                                                                                                                                                                                                                                                                                                                                     |
| --- | ------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1   | Tailoring                                        | A map will show if countries have implemented the fields on EED. It will show if EED was implemented to be used for all procurement procedures, if EED fields are used or not. The necessary field to measure this is `Energy Efficiency Directive (EED)(BT-810)`.                                                                              |
| 2   | Proportions of how EED is applied                | These will be graphs to show how `EED Applicable (BT-810) ` is used.                                                                                                                                                                                                                                                                            |
| 3   | Proportions of labelling and numeric values used | These graphs will show what labelling `Energy Efficiency Label (BT-812)` or what `Energy Consumption in kWh/year (BT-813)` were used.                                                                                                                                                                                                           |
| 4   | Information about energy consumption             | These graphs will show the energy consumption provided in field `Energy Consumption in kWh/year (BT-813)`. The values will be provided as total, average and per quantity provided. Buyers can use this methodology to check how their consumption is compared to other products, services, works and buildings.                                |
| 5   | Information about energy savings                 | These graphs will show the energy savings provided in field `Energy Savings in kWh/year (BT-814)`. For all the values provided, the average will be taken and the range for above and below will be calculated. Buyers can use this methodology to check how their energy savings is compared to other products, services, works and buildings. |

Table: Indicators that will be used for the PPDS EED dashboard

# More information

* **Information on the EED:** [https://energy.ec.europa.eu/news/new-energy-efficiency-directive-published-2023-09-20_en](https://energy.ec.europa.eu/news/new-energy-efficiency-directive-published-2023-09-20_en)
* **The EED:** [https://eur-lex.europa.eu/eli/dir/2023/1791/oj](https://eur-lex.europa.eu/eli/dir/2023/1791/oj)
* **Information about Ecodesign and Energy Label:** [https://energy-efficient-products.ec.europa.eu/ecodesign-and-energy-label_en](https://energy-efficient-products.ec.europa.eu/ecodesign-and-energy-label_en)
* **EPREL - European Product Registry for Energy Labelling:** [https://eprel.ec.europa.eu/screen/home](https://eprel.ec.europa.eu/screen/home)
* **Overview of existing EU Ecodesign, Energy Labelling and Tyre Labelling measures (September 2023):** [https://commission.europa.eu/system/files/2023-09/Summary%20overview%20of%20ED-EL%20measures%20Sept23%20v2-%20for%20web.pdf](https://commission.europa.eu/system/files/2023-09/Summary overview of ED-EL measures Sept23 v2- for web.pdf)
