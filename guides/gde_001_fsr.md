---
title: eForms Guide on the Foreign Subsidy Regulation
date: 18 May 2024
identifier: gde-001-fsr
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Objective

On 12 July 2023, the Foreign Subsidies Regulation (**FSR**) Regulation (EU) 2022/2560 entered into force. The FSR introduces a new set of rules that enables the Commission to address distortions caused by foreign subsidies. This allows the EU to ensure a level playing field for all companies operating in the Single Market, while remaining open to trade and investment.

Starting from 12 October 2023, the notification obligation laid down by the FSR applies. This foresees obligations both for economic operators that submit a bid in a public procurement procedure covered by the FSR[^1], and for the contracting authority that launches the procedure. To support buyers to fulfil this obligation, the Regulation on eForms (EU) 2023/2884 provides dedicated fields.

# Application

The reporting obligation under the FSR requires economic operators that bid in a public procurement procedure to notify foreign financial contributions received in line with Article 28(1) and (2) of the FSR when the value of a public procurement procedure is equal to or greater EUR 250 million. The complete notification or declaration shall be submitted together with the tender to the contracting authority, which shall forward it to the Commission.

At the same time, in line with Article 28(6) of the FSR, buyers that launch a public procurement procedure the value of which net value is equal or greater than EUR 250 million, are required to state in the contract notice that tendering economic operators are under the notification obligation as laid down in Article 29 of the FSR. 

To facilitate the work of buyers, dedicated fields for contract notices and contract award notices are foreseen to be available from the 1 June 2024. Until their availability of the service of the buyer, the information should be provided through the text field `Additional Information BT-300` to indicate the application of the notification obligation under the FSR. In the section “General Approach” this is further defined.

# Fields

| Id            | Name                       | Description                                        | Data type | Repeatable |
|---------------|----------------------------|----------------------------------------------------|-----------|------------|
| BT-681        | Foreign Subsidy Regulation | The Foreign  Subsidies Regulation (FSR) (EU) 2022/2560, in line with  Article 28 thereof, is applicable to  this procurement procedure. | Indicator | No         |

Table: List of fields in competition notices

| Id           | Name                       | Description                                        | Data type | Repeatable |
|--------------| -------------------------- | -------------------------------------------------- | --------- | ---------- |
| BT-681       | Foreign Subsidy Regulation | The Foreign  Subsidies Regulation (FSR) (EU) 2022/2560, in line with  Article 28 thereof, is applicable to  this procurement procedure. | Indicator | No         |
| BG-320       | Tender                     | Information about a tender.  This information differs per lot. In cases such as design contests, some  framework agreements, competitive dialogues and innovation  partnerships, this information may also differ per organisation. | -         | Yes        |
| BT-682       | Foreign Subsidy Measures   | Measures applied  under the Foreign Subsidies Regulation (EU) 2022/2560. | Code      | No         |

Table: List of fields in result notices

# Approach

The FSR is designed as an opt-in. This means that only for public procurement procedures, which fall under the FSR it is necessary to indicate `yes` under `Foreign Subsidy Regulation BT-681`, otherwise the default should be `no`.

If the field is not available in the service, the buyer should encode in the field `Additional Information (BT-300)` the following wording: **`BT-681 This procedure falls under the Foreign Subsidy Regulation (FSR)`**.

If it is indicated that the public procurement procedure in question falls under FSR, with the result notice more information needs to be provided by using the code list `Foreign Subsidy Measures BT-682`. This is applicable for all tenders listed if `Foreign Subsidy Regulation (BT-681)` is indicated as `Yes` during the competition notice. The specific FSR measures as mentioned in the Foreign Subsidies Regulation (FSR) Regulation (EU) 2022/2560 are:

* Notification submitted, decision with no objection
* Notification submitted, decision prohibiting the award 
* Notification submitted, decision on irregular tender or request to participate
* Notification submitted, decision with commitments
* Notification submitted, administrative closure of FSR preliminary review procedure
* Declaration submitted, standard award procedure
* Declaration submitted, standard MEAT award

If the dedicated fields for the FSR are not available in the service, the buyer should encode in the field `Additional Information (BT-300)` the correct item from the above list, like : **`BT-682 Notification submitted, no objection decision`**

The buyer will receive the information from the European Commission, which code should be selected for each tenderer.

# Code lists

| Id   | Field                           | Link                                                         |
| ---- | ------------------------------- | ------------------------------------------------------------ |
| 1    | Foreign Subsidy Measures BT-682 | Website: [cdl-001-fsr](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_001_fsr.md), PDF: [cdl-001-fsr](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_001_fsr.pdf) |

Table: List of code lists used

# Business rules

| Id   | Description                                                  |
| ---- | ------------------------------------------------------------ |
| 1    | The Foreign Subsidies Regulation only applies to procedures, that fall under the following EU Directives 2014/23/EU, 2014/24/EU and 2014/25/EU. |
| 2    | The Foreign Subsidies Regulation only applies to procedures, where the estimated value (BT-27) or Framework Maximum Value (BT-271) is equal or higher than EUR 250 million. |
| 3    | A foreign subsidy measure must be selected (BT-682), because this procedure falls under the Foreign Subsidy Regulation (BT-681). |

Table: List of business rules

# Tailoring

| Id   | Field                             | Description                                                  |
| ---- | --------------------------------- | ------------------------------------------------------------ |
| 1    | Foreign Subsidy Regulation BT-681 | This field is mandatory and must be therefore implemented.   |
| 2    | Foreign Subsidy Measures BT-682   | This field is conditionally mandatory and must be therefore implemented. |
| 3    | Tender BG-320                     | It is necessary to provide the `Foreign Subsidy Measure BT-682` only for the winning tenderer. To increase transparency, with eForms it is possible to provide this information for all tenderers. This will help buyers and policy makers to understand participation of tenderers who receive foreign subsidies. |

Table: Possibilities for tailoring

# Analysis

The Public Procurement Data Space (PPDS) will provide a dashboard to report on the data provided by buyers on FSR. The following table shows the indicators related to FSR.

| Id   | Name                              | Description                                                  |
| ---- | --------------------------------- | ------------------------------------------------------------ |
| 1    | General ratio of FSR to non-FSR procedures | This indicator shows how often public procurement procedures are  falling under the FSR compared to all EU procedures. |
| 2    | FSR measures                               | This indicator shows which of the FSR measures are used for the  different procedures. As there can be multiple FSR measures per procedures, it  will be used against all observations. |
| 3    | Multiplicity of FSR measures               | This indicator shows if one, two or more FSR measures were used in  a procurement procedure. |

Table: List of indicators

The indicators are not only based on the fields specifically for FSR, but also other fields in eForms. The following table shows all the additional fields that will be used for the FSR dashboard. They will usually be offered as filter options.

| Id      | Name                           | Description                                                                                                   |
|---------|--------------------------------|---------------------------------------------------------------------------------------------------------------|
| BT-04   | Procedure Identifier           | This field is important to link public procurement procedures to  the overall procedure.                      |
| BT-01   | Procedure Legal Basis          | This field is important to show the number of observations per  legal basis.                                  |
| BT-501  | Organisation Identifier        | This field is relevant to link the buyer or tenderer to the IPI  cases.                                       |
| BT-514  | Organisation Country Code      | This field links the location to the organisation to the IPI  cases.                                          |
| BT-08   | Organisation Role              | This field is important to understand the role of the  organisation.                                          |
| BT-27   | Estimated Value                | This field is important to compare the monetary aspect to the FSR  cases.                                     |
| BT-23   | Main Nature                    | This field is important to understand if the FSR case is linked to  services, supply, works or a combination. |
| BT-262  | Main Classification Code       | This field is important to link the FSR case to the CPV-Code.                                                 |
| BT-5141 | Place Performance Country Code | This field is important to link the FSR case to the place of  performance.                                    |
| BT-1374 | Funds Lot Identifier           | This field is important to link the FSR case EU funds.                                                        |
| BT-720  | Tender Value                   | This field shows the actual tender value (at least from the winning  tenderer).                               |

Table: List of fields for enriching the indicators

# More information

* Link: https://competition-policy.ec.europa.eu/foreign-subsidies-regulation_en 
* Law: https://eur-lex.europa.eu/eli/reg/2022/2560/oj
* Contact: [GROW-FSR-PP-NOTIFICATIONS@ec.europa.eu](mailto:GROW-FSR-PP-NOTIFICATIONS@ec.europa.eu)

------

[^1]: For more information on the scope of application of the FSR concerning public procurement procedures, see Article 2(3) FSR.
