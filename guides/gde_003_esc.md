---
title: eForms Guide on Exclusion Grounds and Selection Criteria
date: 18 May 2024
identifier: gde-003-esc
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Objective

The objective of using exclusion grounds and selection criteria in public procurement procedures is to ensure that the procurement process is fair, transparent, and results in the award of contracts to suppliers that are best suited to deliver the required goods, works or services.

# Fields

| Id         | Name                                     | Description                                                  | Data type | Repeatable |
| ---------- | ---------------------------------------- | ------------------------------------------------------------ | --------- | ---------- |
| **BG-700** | Exclusion Grounds and Selection Criteria | Information about the exclusion grounds and selection criteria used for this procurement procedure. | -         | No         |
| BT-806     | Exclusion Grounds Source                 | Where the exclusions grounds are *described*, for example, the procurement documents *, the notice* or in *the* ESPD. | Code      | Yes        |
| **BG-701** | Exclusion Grounds                        | The description of criteria regarding the personal situation of tenderers that may lead to their exclusion. This may also include specific national exclusion grounds. | -         | Yes        |
| BT-67(a)   | Exclusion Ground                         | The *exclusion ground*. | Code      | No         |
| BT-67(b)   | Exclusion Ground Description             | The description of the exclusion *ground*. | Text      | No         |
| BT-821     | Selection Criteria Source                | Where the selection criteria are defined, for example, the procurement documents *, the notice* or in *the* ESPD. | Code      | Yes        |
| **BG-702** | Selection Criteria                       | Information about the selection criteria. All criteria must be listed. This information may differ per lot. | -         | Yes        |
| BT-809     | Selection Criterion               | The *selection* criterion.            | Code      | No         |
| BT-750     | Selection Criteri*on* Description   | The description of the selection criterion, and how the criterion will be used to select candidates to be invited for the second stage of the procedure (if a maximum number of candidates was set). | Text      | No         |

Table: List of fields in planning and competition notices

# Approach

The way of how Exclusion Grounds and Selection Criteria can be provided in eForms, as of the Regulation (EU) 2023/2884 will make it much easier for buyers to provide information and more explicit. The way it is handled for Exclusion Grounds and Selection Criteria is similar except for the difference, that Exclusion Grounds are provided only once per procedure, while Selection Criteria have to be provided for each lot.

First the buyer needs to select `Exclusion Grounds Source BT-806` and `Selection Criteria Source BT-821` indicating where the supplier can find information.  The buyer can choose one or more from the following possibilities:

* **ESPD:** If the buyer selects this, it means that the supplier will find the Exclusion Grounds and/or Selection Criteria in the European Single Procurement Document. No more information needs to be provided in the notice. Nevertheless, the ESDP service could fill in the data in the notice also for transparency automatically. In this case, also `Notice` should be selected.
* **Notice:** If the buyer selects this, it means that the supplier will find the Exclusion Grounds and/or Selection Criteria in this notice.
* **Procurement Document:** If the buyer selects this, it means that the supplier will find the Exclusion Grounds and/or Selection Criteria in the procurement documents. No more information needs to be provided in the notice. However, it is possible in the fields `Exclusion Ground Description BT-67(b)` and/or `Selection Criteria Description BT-750` to provide in the text field the information to the supplier where they can find the information in the procurement documents.

If `Notice` is selected, then each Exclusion Ground should be listed in the notice. For the different lots, the Selection Criteria that are necessary to select the supplier may be listed. An entry for an Exclusion Ground or Selection Criterion is made out of two fields. For each `Exclusion Ground BT-67(a)` an `Exclusion Ground Description BT-67(b)` can be given. While it is possible to manually enter each Exclusion Ground, it is advisable that this is done by an ESPD service or from the eProcurement service automatically as they usually don't change to make it easier for the buyer. It is similar for Selection Criteria. For each `Selection Criterion BT-809` a `Selection Criterion Description BT-750` must be given. If an ESPD service is used, then this information could also be filled in automatically.

# Code lists

| Id   | Field                                                        | Link                                                         |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1    | Exclusion Grounds Source BT-806 and Selection Criteria Source BT-821 | Website: [cdl-002-dpp](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_002_dpp.md), PDF: [cdl-002-dpp](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_002_dpp.pdf) |
| 2    | Exclusion Ground BT-67(a)                                    | Website: [cdl-003-exg](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_003_exg.md), PDF: [cdl-003-exg](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_003_exg.pdf) |
| 3    | Selection Criterion BT-809                                   | Website: [cdl-004-slc](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_004_slc.md), PDF: [cdl-004-slc](https://code.europa.eu/eproc/eforms/docs/-/blob/main/codelists/cdl_004_slc.pdf) |

Table: List of code lists used

# Business rules

| Id   | Description                                                  |
| ---- | ------------------------------------------------------------ |
| 1    | Any combination of `ESPD`, `Notice` and `Procurement Document` is allowed. |
| 2    | If `Notice` is selected as a source in `BT-806` or `BT-821`, then at least one Exclusion Ground or Selection Criterion `BT-67(a)` or `BT-809` must be included. |
| 3    | For each selected `Selection Criteron BT-809` a `Selection Criterion Description BT-750` must be provided. |

Table: List of business rules

# Tailoring

| Id       | Name                            | Tailoring                                                    |
| -------- | ------------------------------- | ------------------------------------------------------------ |
| BT-806   | Exclusion Grounds Source        | This field is mandatory and must be therefore implemented.   |
| BT-67(a) | Exclusion Ground                | If the Exclusion Grounds will never be provided in the notice, then this field can be tailored out. In this case, the code list on `Exclusion Grounds Source` must be tailored to allow only the option of `ESPD` and `Procurement Document`. Please bear in mind, that this will take away the possibility to encode `Exclusion Grounds` in the notice and reduce transparency. |
| BT-67(b) | Exclusion Ground Description    | If `BT-67(a)` will not be used, it might also be better not to provide this field. |
| BT-821   | Selection Criteria Source       | This field is mandatory and must be therefore implemented. **Tip:** While this must be set for each lot individually in eForms, the user interface can be adjusted to set it only once for `Selection Criteria`. |
| BT-809   | Selection Criterion             | If the Selection Criteria will never be provided in the notice, then this field can be tailored out. In this case, the code list on `Selection Criteria Source` must be tailored to allow only the option of `ESPD` and `Procurement Document`. Please bear in mind, that this will take away the possibility to encode `Selection Criteria` in the notice and reduce transparency. |
| BT-750   | Selection Criterion Description | If `BT-809` will not be used, it might also be better not to provide this field. |

Table: Possibilities for tailoring

# Analysis

| Id   | Name                                  | Description                                                  |
| ---- | ------------------------------------- | ------------------------------------------------------------ |
| 1    | Tailoring                                                    | A map will show if and how Countries have tailored Exclusion Grounds and Selection Criteria, meaning that the existing of the fields `BT-67(a)`, `BT-67(b)`, `BT-809` and `BT-750`. The ratio of existing of the fields to the number of notices will show how this was implemented in each country. |
| 2    | General ratio of combinations used for Exclusion Grounds and Selection Criteria | There will be three values showing the number of combinations used for All, Exclusion Grounds and Selection Criteria. This will give an understanding on how the buyer communicates Exclusion Grounds and Selection Criteria to Suppliers. |
| 3    | Use of Exclusion Grounds                                     | If it is indicated that the Exclusion Grounds will be provided in the `Notice`, this value will list the number of occurrences for each Exclusion Ground. There should not be much variation within a country. |
| 4    | Use of Selection Criteria                                    | If it is indicated that the Selection Criteria will be provided in the `Notice`, this value will list the number of occurrences for each Selection Criteria given in notices. It will be interesting to see for what types of procedure what selection criteria are selected. Combined with the information of for example `Received Submissions Count BT-759` it might be possible to identify how selection criteria can impact competition. |
| 5    | Use of Selection Criteria Description                        | If it is indicated that the Selection Criteria will be provided in the `Notice`, AI will be used with the aim of understanding how the buyer has set the Selection Criteria. |

Table: Indicators that will be used for the dashboard

| Id      | Name                           | Description                                                  |
| ------- | ------------------------------ | ------------------------------------------------------------ |
| BT-04   | Procedure Identifier           | This field is used to link public procurement procedures to  the overall procedure. |
| BT-01   | Procedure Legal Basis          | This field is important to show the number of occurrences per  legal basis. |
| BT-501  | Organisation Identifier        | This field is relevant to link to the buyer.                 |
| BT-514  | Organisation Country Code      | This field links the location to the country of the buyer.   |
| BT-27   | Estimated Value                | This field is used for comparissons to the estimated value.  |
| BT-23   | Main Nature                    | This field is important to understand if is linked to  services, supply, works or a combination. |
| BT-262  | Main Classification Code       | This field is important to link it to the CPV-Code.          |
| BT-5141 | Place Performance Country Code | This field is important to link it to the place of  performance. |
| BT-1374 | Funds Lot Identifier           | This field is important to understand if it is linked to an EU funds. |
| BT-720  | Tender Value                   | This field shows the actual tender value (at least from the winning  tenderer). |
| BT-759  | Received Submissions Count     | This will show the number of tenders submitted.              |
| BT-760  | Received Submissions Type      | This will show for example if SME participated.              |

Table: Additional fields used for the dashboard 

# More information

* Contact: [GROW-EPROCUREMENT@ec.europa.eu](mailto:GROW-EPROCUREMENT@ec.europa.eu)
