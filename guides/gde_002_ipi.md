---
title: eForms Guideline on the International Procurement Instrument 
date: 18 July 2024
identifier: gde-002-ipi
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Objective

On 29 August 2023, the International Procurement Instrument (‘IPI’) [Regulation (EU) 2022/1031](https://eur-lex.europa.eu/eli/reg/2022/1031/oj) entered into force. The IPI introduces measures limiting non-EU companies’ access to the open EU public procurement market if their governments do not offer similar access to public tenders to EU companies seeking business. By fostering reciprocity, this tool aims to open these protected markets and to end the discrimination against EU companies in third countries.
Reporting obligations for buyers on this Regulation are mentioned in Article 13(2). According to Article 1(2) IPI applies to public procurement procedures falling under 2014/23/EU, 2014/24/EU, 2014/25/EU. To support buyers to fulfil this obligation, the Regulation on eForms (EU) 2023/2884 provides dedicated fields.


# Application

The implementing act imposing IPI measures will be proportionate and targeted to address specific market access barriers faced by the EU industry in the third countries concerned. The IPI measures will apply to a limited number of contracts. The implementing act will contain all the parameters necessary to identify these contracts subject to IPI measures: targeted countries, applicable thresholds, contracting authorities concerned, CPV codes concerned.
In addition, the implementing act will define the measure to be applied: exclusion of the bidder or application of a score adjustment measure. The score adjustment measures will be precisely defined including the value of the adjustment.

# Fields

| Id       | Name                                                         | Description                                                  | Type | Rep. |
| -------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ---- | ---- |
| *BG-684* | *International Procurement Instrument (IPI)*                 | *Section on International Procurement Instrument (IPI)*      | -    | No   |
| BT-684   | ~~International Procurement Instrument~~*IPI* Measures are Applicable | Measures under the International Procurement Instrument (IPI) Regulation (EU) 2022/1031 are applicable to this procurement procedure. | Ind  | No   |
| BG-681   | Information on IPI Measures                                  | The list of IPI Measures that were applied to this tender.   | -    | Yes  |
| BT-685   | Specific IPI Measure                                         | Information on the IPI Measure applicable to this tender.    | Code | No   |
| BT-686   | Number of tender applications that fall under this IPI Measure | Number of requests to participate and tenders to which this IPI Measure was applied, in the case of a public contract; number of requests to participate and projects, in the case of a design contest. | Num  | No   |
| BT-687   | Exception to the application of the IPI Measure              | Exception from the application of the IPI Measure.           | Code | No   |
| BT-688   | Overriding reasons relating to the public interest           | A description and justification for the application of overriding reasons relating to the public interest. | Text | No   |

Table: List of fields for result notices

# Approach

The IPI is mandatory to apply, if an IPI measure is applicable. In this case, it is necessary to indicate `Yes` under `International Procurement Instrument Measures Applicable (BT-684)` otherwise the default should be `No`.

If it is indicated that an IPI measure is applicable to this EU public procurement procedure, more information needs to be provided under the business group `Information on IPI Measures (BG-681)`. It is necessary to provide the `Number of tender applications that fall under this IPI Measure (BT-686)` for each `Specific IPI Measure (BT-685)`. The specific IPI measures are mentioned through Implementing Acts. As there is currently no Implementing Act, the code list will contain one entry `Empty - There is currently no Implementing Act`. The code list will be updated as soon as there is an Implementing Act.

As it is possible to have different types of `Specific IPI Measure (BT-685)`, it is possible to provide multiple measures and to mention for each of them the `Number of tender applications that fall under this IPI Measure (BT-686)`. It is also possible that it is necessary to have an `Exception to the application of the IPI Measure (BT-687)`. The list includes:

1. **No exception**: This is the default value, for a `Specific IPI Measure (BT-685)`.

2. **There was only one tender:** In this case, as the name indicates, only one tender was submitted.

3. **Overriding reasons relating to the public interest:** If this is selected, then it is also necessary to provide a justification under `Overriding reasons relating to the public interest (BT-688)` 

# Code lists

| Id   | Field                                                        | Link |
| ---- | ------------------------------------------------------------ | ---- |
| 1    | `Specific IPI Measure (BT-685)` will include the value `Empty - There is currently no Implementing Act` as there is currently no Implementing Act. | -    |
| 2    | `Exception to the application of the IPI Measure (BT-687)` will include the following three entries `No exception`, `There was only one tender` and `Overriding reasons relating to the public interest`. | -    |

Table: List of code lists used

# Business rules

| Id   | Description                                                  |
| ---- | ------------------------------------------------------------ |
| 1    | The International Procurement Instrument only applies to procedures, that fall under the EU Directives 2014/23/EU, 2014/24/EU and 2014/25/EU. |
| 2    | If `International Procurement Instrument Measures are Applicable (BT-684)` is selected, then at least one `Specific IPI Measure (BT-685)` and the `Number of tender applications that fall under this IPI Measure (BT-686)` must be provided . Also, a code from `Exception to the application of the IPI Measure (BT-687)` must be selected. |
| 3    | If for `Exception to the application of the IPI Measure (BT-687)` the entry `Overriding reasons relating to the public interest` is selected, then also a justification must be given in the field `Overriding reasons relating to the public interest (BT-688)`. |

Table: List of business rules

# Tailoring

| Id     | Name                               | Tailoring                                                    |
| ------ | ---------------------------------- | ------------------------------------------------------------ |
| BT-684 | International Procurement Instrument Measures are Applicable | As the International Procurement Instrument is mandatory to use, all fields have to be implememented. As there is currently no Implementing Act, the default value for the field `BT-684` must be `No`. No other information should be given. |

Table: Possibilities for tailoring

# Analysis

| Name                                         | Description                                                                                                                                                                                                |
|----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| General ratio of IPI to non-IPI procedures   | This indicator shows how often public procurement procedures are falling under the IPI compared to all EU procedures.                                                                                      |
| IPI measures                                 | This indicator shows which of the IPI measures are used for the different procedures. As there can be multiple IPI measures per procedures, it will be used against all observations.                      |
| Multiplicity of IPI measures                 | This indicator shows if one, two or more IPI measures were used in a procurement procedure.                                                                                                                |
| Number of tender applications                | This indicator will show the sum of tender applications, that fall under the IPI.                                                                                                                          |
| Exception to the application of IPI measures | This indicator will show the ratio of procedures that fall under the IPI compared to the type of exception.                                                                                                |
| Reasons for the exception                    | Here the buyer should provide a justification for the reasons relating to the public interest. As this is a free text, AI will be used to categorize the reasons, to make it representable in a dashboard. |

Table: Indicators that will be used for the PPDS dashboard

# More information

* **Information on IPI:** https://trade.ec.europa.eu/access-to-markets/en/content/international-public-procurement-instrument 
* **Regulation on the IPI:** https://eur-lex.europa.eu/eli/reg/2022/1031/oj

