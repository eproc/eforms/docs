---
title: eForms Guide on Review
date: 13 October 2024
identifier: gde-005-rew
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: |
  \usepackage{plex-serif}
  \usepackage[left]{lineno}
  \linenumbers
---

# Objective

The Member States shall ensure that the review procedures are available, under detailed rules which the Member States may establish, at least to any person having or having had an interest in obtaining a particular public supply, services or public works contract and who has been, or risks being harmed by an alleged infringement. 

Member States may require that the person seeking the review must have previously notified the buyer of the alleged infringement and of their intention to seek review.

There could be various outcomes of the review. If the review body finds the application for review founded, it may set aside the contracting authority/entity decision,  request that certain parts of the award procedure are repeated or even cancel the entire public procurement procedure. 

Throughout the review procedure the review body will declare a contract ineffective, shorten it or fine the buyer if:

* the buyer awarded a contract without prior publication of the contract notice when publication was required,
* a standstill period was required but not respected,
* there is another breach of public procurement rules.

For transparency reasons, the information on the review procedure can also be published through eForms. The dedicated fields allow the different stages of the review procedure as well as the outcome of such application for reviews to be captured. There is no legal obligation to publish information on the review procedure.

# Fields

| Id         | Name                             | Description                                                                                                                                                                                                                                          | Type       | Rep. | Use         |
| ---------- | -------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------- | ---------- | ---- | ----------- |
| **BG-716** | Review                           | Information about a review request or a review decision.                                                                                                                                                                                             | -          | Yes  | O           |
| *BT-804*   | Review Technical Identifier      | A technical identifier linked to this review information                                                                                                                                                                                             | Id         | No   | M           |
| BT-783     | Review Request or Decision       | Whether  the information concerns a review request or a review decision.                                                                                                                                                                             | Code       | No   | ~~O~~ *CM*  |
| BT-784     | Review Identifier                | An  identifier of a review request or a review decision. When appealed to a  higher review body instance, a review requests must receive a new identifier.                                                                                           | Id         | No   | CM          |
| BT-785     | Review Previous Identifier       | An  identifier of the review request(s) that led to this decision or a review  decision that is being appealed by this review request. Review decision which  was initiated by the review body ("ex officio") are not preceded by  a review request. | Id         | No   | EM          |
| BT-786     | Review Notice Section Identifier | An  identifier of one or more ~~to~~ *of* a section~~s~~ within this notice. The information in the  review section refers to this section or these sections.                                                                                        | Id         | Yes  | ~~CM~~ *O*  |
| *BT-808*   | Review Applicant Identifier      | The identifier of the applicant.                                                                                                                                                                                                                     | Id         | Yes  | O           |
| BT-787     | Review Date                      | The date ~~and time~~ when the review request was submitted or the review decision was  taken.                                                                                                                                                       | Date       | No   | CM          |
| BT-788     | Review Title                     | The ~~name~~ *title* of the review request or review decision.                                                                                                                                                                                       | Text       | No   | CM          |
| BT-789     | Review Description               | The  description of the review request or review decision or any other additional  information.                                                                                                                                                      | Text       | No   | CM          |
| BT-799     | Review Body Type                 | The type of review body (e.g. buyer; first instance review body; second instance review body; other, e.g. a civil court setting damages).                                                                                                            | Code       | No   | CM          |
| *BT-807*   | Review Body Identifier           | The review body (e.g. buyer; first instance review body; second instance review body; other, e.g. a civil court setting damages).                                                                                                                    | Identifier | No   | O           |
| BT-790     | Review Decision Type             | The  decision type (e.g. accepted, rejected because the complainant did not have  legal standing).                                                                                                                                                   | Code       | Yes  | CM          |
| BT-791     | Review Irregularity Type         | The type*s* of irregularity alleged in the review request or confirmed in the review  decision.                                                                                                                                                      | Code       | Yes  | CM          |
| BT-792     | Review Remedy Type               | The  remedy (e.g. interim measure*s, setting aside of a decision*, damages) requested in the review request or  applied by the review decision.                                                                                                      | Code       | Yes  | CM          |
| BT-793     | Review Remedy Value              | The  value of the remedy (e.g. damages, fines).                                                                                                                                                                                                      | Value      | No   | ~~CM~~*EM*  |
| BT-794     | Review URL                       | The  uniform resource locator (e.g. the web address) of the documents concerning  the review request or a review decision.                                                                                                                           | URL        | No   | O           |
| BT-795     | Review Request Fee               | The fee  paid for lodging the review request.                                                                                                                                                                                                        | Value      | No   | O           |
| BT-796     | Review Request Withdrawn         | The  review request was withdrawn.                                                                                                                                                                                                                   | Ind        | No   | ~~CM~~ *EM* |
| BT-797     | Review Request Withdrawn Date    | The date ~~and time~~ when the request for review was withdrawn.                                                                                                                                                                                     | Date       | No   | CM          |
| BT-798     | Review Request Withdrawn Reasons | The  reasons for withdrawing the request for review.                                                                                                                                                                                                 | Text       | No   | O           |

Table: List of fields in result and completion notices

# Approach

## General

The section on `Review (BG-716)` is designed as opt-in, meaning that it can be used. This business group contains all fields connected to reviews. It is repeatable, allowing the buyer to provide information about multiple requests or decisions for the same procedure. If an update of a review procedure is provided, such as a decision was appealed and reviewed at a higher level, a new notice must be published. This should not be done through changing an existing notice.

## Review

Under the field `Review Request or Decision (BT-783)` buyers can select whether the information concerns a review request or a review decision.

## Identification of the review

The `Review Technical Identifier (BT-804)` is mandatory as it is necessary in the SDK to link to this section. It is generated automatically by the eProcurement service and will have the following pattern `REV-xxxx`.

The field `Review Identifier (BT-784)` captures the specific identifier for the review request or decision. When appealed to a higher review body instance, a review requests/ decisions must receive a new identifier, this can also be used for interim measures. In those cases, where there has been a previous request/decision the field `Review Previous Identifier (BT-785)` should be filled in with the identifier of the review request that led to this decision or a review decision that is being appealed by this review request. Review decisions which are initiated by the review body ("ex officio") are not preceded by a review request.

It is also possible to link the review to sections, like a specific lot, of the notice through `Review Notice Section Identifier (BT-786)`.

## Details of the Review Request or Review Decision

It is necessary to provide the following information for each request or decision:

* The `Review Technical Identifier (BT-804)`.
* The `Review Date (BT-787)`, which is the date of the submission of the review request or the date when review decision was taken.
* The field `Review Title (BT-788)` captures the title of the review request or review decision.
* A description of the review procedure must be provided in the field `Review Description (BT-789)`.
* The review body needs to be selected through `Review Body Type (BT-799)`.

It is optional to provide:

* The applicant through `Review Applicant Identifier (BT-808)`.
* The review body through `Review Body Identifier (BT-807)`. 

## Details of the Review Types

In the case of a decision, the type of the decision (e.g. accepted, rejected because the complainant did not have legal standing or other) the field `Review Decision Type (BT-790)` must be provided. The type of irregularity must be provided in the field `Review Irregularity Type (BT-791)`. The information about the remedy type must be provided by selecting the value from the list `Review Remedy Type (BT-792)`. It is possible to provide more than one remedy type. 

## Other Information 

The field `Review Remedy Value (BT-793)` is used for providing information on the value of the remedy (e.g. damages, fines). The remedy value should be provided, if this information is available to the buyer. The two fields `Review URL (BT-794)` and `Review Fee (BT-795)` are both optional. In `Review URL (BT-794)` the buyer can provide the web address of the documents concerning the review request or a review decision. The fee for the lodging the request can be provided in the field `Review Fee (BT-795)`.

## Withdrawal of the Review Request

There is also the possibility to provide information, in case the application for review request  was withdrawn. The first field `Review Request Withdrawn (BT-796)` is an indicator and must be filled out with `Yes`. In this case, also the date must be provided in field `Review Request Withdrawn Date (BT-797)`. It is optional to provide more information for the reasons of the withdrawal under the field `Application Withdrawn Reasons (BT-798)`.

# Code lists

| Id  | Field                                                                                     | Link                                                                                                                                                                      |
| --- | ----------------------------------------------------------------------------------------- | -------------------------------------------- |
| 1   | `Review Request or Decision (BT-783)` will have only two values `request` and `decision`. | Website: -                                                                                                                                                                |
| 2   | `Review Decision Type (BT-790)`                                                           | Website: [EU Vocabulary](https://op.europa.eu/en/web/eu-vocabularies/concept-scheme/-/resource?uri=http://publications.europa.eu/resource/authority/review-decision-type) |
| 3   | `Review Request Irregularity Type (BT-791)`                                               | Website: [EU Vocabulary](https://op.europa.eu/en/web/eu-vocabularies/concept-scheme/-/resource?uri=http://publications.europa.eu/resource/authority/irregularity-type)    |
| 4   | `Review Remedy Type (BT-792)`                                                             | Website: [EU Vocabulary](https://op.europa.eu/en/web/eu-vocabularies/concept-scheme/-/resource?uri=http://publications.europa.eu/resource/authority/remedy-type)          |

Table: List of code lists used

# Business rules


| Id  | Description                                                                                                                                                                                                                                                                                                                                                                       |
| --- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1   | If the buyer provides information on a review request or review decision, the eProcurement service must provide a `Review Technical Identifier (BT-804)`. It needs to be provided for each request or decision, to be able to link to those sections within a notice.                                                                                                             |
| 2   | If `Review Technical Identifier (BT-804)` is added, then it is also necessary to provide the following information  `Review Request or Decision (BT-783)`,  `Review Identifier (BT-784)` ,  `Review Date (BT-787)`, `Review Title (BT-788)`, `Review Description (BT-789)`,  `Review Body Type (BT-799)`,  `Review Irregularity Type (BT-791)` and `Review Remedy Type (BT-792)`. |
| 3   | If `Review Request or Decision (BT-783)` is `decision` then also `Review Decision Type (BT-790)` must be given.                                                                                                                                                                                                                                                                   |
| 4   | If `Review Request Withdrawn (BT-796)` is `yes` then ` Review Request Withdrawn Date (BT-797)` must be given.                                                                                                                                                                                                                                                                     |

Table: List of business rules

# Tailoring

| Id     | Name               | Tailoring                                                    |
| ------ | -------------------| ------------------------------------------------------------ |
| BG-716 | Review | The section on review is optional and can be tailored out. |

Table: Possibilities for tailoring

# Analysis

| Id   | Name                             | Description                                                  |
| ---- | -------------------------------- | ------------------------------------------------------------ |
| 1    | Tailoring                        | A map will show if the information on how review is used.    |
| 2    | Proportion of Irregularity Types | The proportion of requested irregularity types will be shown in comparison to the number of reviews submitted. |
| 3    | Proportion of Decision Types     | The proportion of decision types will be shown in comparison to the number of reviews submitted. |
| 4    | Proportion of Remedy Types       | This will show the proportion of the different remedy types (interim measures, setting aside of a decision, damages). |
| 5    | Proportion of Withdrawn Requests | This will show the proportion on requests withdrawn by the applicant. |

Table: Indicators that will be used for the PPDS Review dashboard

# More information

* **Law:** [EU Directive on procurement in the defence and security sector,](https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1415011920358&uri=CELEX:32009L0081) [EU Directive on remedies for the public sector,](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:01989L0665-20140417) [EU Directive on remedies for the utilities sector](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:01992L0013-20140417)

* **Contact:** [GROW-EPROCUREMENT@ec.europa.eu](mailto:GROW-EPROCUREMENT@ec.europa.eu)
