---
title: eForms Governance (Draft)
author: DG GROW.G4 eProcurement Team
date: 05 February 2025
identifier: eforms-governance
output: pdf_document
lang: en-GB
papersize: a4
linestretch: 1.3
geometry: margin=2cm
colorlinks: true
header-includes: \usepackage{plex-serif}
---

# 1. Introduction

## 1.1. eForms

1. eForms are an European Union legislative standard for procurement data used by public buyers to publish notices in the Supplement to the Official Journal of the EU on Tenders Electronic Daily. Commission Implementing Regulation [(EU) 2019/1780](https://eur-lex.europa.eu/eli/reg_impl/2019/1780/oj) established these standard forms (eForms) for use in the publication of notices in the field of public procurement, as outlined in the Annex of the Regulation.
2. eForms serve multiple functions; besides their primary role in publishing notices on TED, they are also used for data collection and have the potential to support policy makers and a large number of re-users for commercial, research and other use cases.
3. Well-implemented eForms will increase economic operators’ ability to find relevant notices; reduce administrative burdens for buyers; increase governments’ capabilities to make data-driven decisions about public spending as well as make public procurement more transparent for citizens.
4. eForms must be implemented in national eProcurement systems. They can – and should – be tailored to national needs, resulting in outputs that are customised but still are compliant with the technical specifications. This means they are not an “off the shelf” legislation that policy makers can hand over for implementation to IT departments. Instead, before developers begin any work, procurement policy decision-makers need to sit all stakeholders around the table and decide on how various attributes of eForms should be implemented.
5. Decisions on how eForms are tailored can often be derived out of the data itself. Nevertheless, to compare data between countries better, it is also beneficial to understand the rationale behind each country's tailoring decisions.
6. Good implementation of eForms is an investment. It requires sufficient time and resources but will yield considerable returns in time saved for all implementers and users. Unlike previous standard forms, eForms are intended to be to a largely completed automatically by eProcurement systems, not by the users, thus significantly reducing administrative burden.
7. The tailoring process requires addressing questions such as, for example, “Should eForms be used below-threshold?”, “Should notices be published for contracts based on framework agreements?”, “Are there any types of notices we do not need?”, “Which optional fields shall become mandatory?”, “Which optional fields shall not be used at all?”, or “Should buyers be able to provide information about review decisions?”.
8. As foreseen in the Implementing Regulation [(EU) 2019/1780](https://eur-lex.europa.eu/eli/reg_impl/2019/1780/oj) and due to requirements from Member States and sectoral legislation have been adopted.
9. In November 2022, the Commission adopted the Implementing Regulation [(EU) 2022/2303](https://eur-lex.europa.eu/eli/reg_impl/2022/2303/oj). The first amendment addressed the requests on several topics, such as reporting requirements under the Clean Vehicles Directive, Green Public Procurement, EU funds and framework agreements.
10. In December 2023, the European Commission adopted Implementing Regulation [(EU) 2023/2884](https://eur-lex.europa.eu/eli/reg_impl/2023/2884/oj). The second amendment addresses the requirements stemming from sectoral legislation to include fields for International Procurement Instruments, Foreign Subsidies Regulation, the Energy Efficiency Directive and they also includes voluntary forms.

## 1.2. Objective

1. The aim of this governance model is to establish a robust framework for the management and implementation of eForms, fostering collaboration and transparency for future amendments and updates. This document is primarily intended for policy makers and implementers. It provides a framework that outlines the levels and participants involved in the process, summarises the various artefacts and their purposes, and details the procedures for amendments.
2. The environment of public procurement is undergoing substantial changes due to the impact of sectoral legislation and the future revision of the Public Procurement EU legal framework. As eForms become increasingly utilised for reporting and monitoring purposes, it is crucial to anticipate and adapt to these changes effectively. This governance model aims to shape the future of eForms by:
	1. Developing a clear and strong framework for managing and implementing eForms to maximise their potential.
	2. Ensuring adaptability to anticipate the effects of sector-specific legislation and future changes in the EU Public Procurement legal framework.
	3. Improving efficiency, accuracy, and compliance with the EU legal framework for Public Procurement.

## 1.3. Scope

1. The governance model focuses on eForms, encompassing both amendments to the Implementing Regulation and updates between amendments. While eForms are linked to the European Single Procurement Document (ESPD), eCertis and the eProcurement ontology, this governance model does not cover these aspects.

## 1.4 General principles

1. The eForms governance will be revisited and discussed annually by the EXEP to ensure it remains fit the purpose, facilitating a transparent and collaborative approach to governing eForms.
2. The aim of the eForms Governance is to be transparent for all actors and allow for discussions with the eForms Community and EXEP.
3. The procedure to amend the eForms Implementing Regulation will commence at the beginning of a year, with adoption at the end of the year. It is planned to have an amendment procedure every second year.
4. Annually, the governance model should be reviewed and adjusted if needed, utilising the EXEP meeting in autumn for discussions. Comments will be considered in agreement with participants from EXEP members, OP and DG GROW.
6. The Implementing Regulation and its Annex will lead the technical implementation.
7. Artefacts for the technical implementation are published using a versioned Software Development Kit (SDK). Each version may be employed for at least a year following its publication. The lifespan of SDK versions will be discussed, if necessary, by EXEP, GROW and OP.

# 2 Actors and roles

1. For following figure depicts all main actors involved in the governance of eForms.

![Figure on the governance actors](./images/eforms_actors.png)

## 2.1 Levels

### 2.1.1. Strategic level

1. Sets the overall strategy of the evolution of eForms implementation.
2. Defines the long-term strategy.

### 2.1.2. Tactical level

1. Supports release management of eForms including working on guides, policy issues and code lists.

### 2.1.3. Operational level

1. Supports the overall implementation of eForms.
2. Supports implementers and users of the eForms applications.

## 2.2. Formal and informal groups

### 2.2.1. Formal groups

1. Throughout the eForms amendments procedure two types of formal groups will be involved namely one comitology and two expert groups.

#### 2.2.1.1. Comitology

1. EU laws sometimes authorise the European Commission to adopt implementing acts, which establish conditions that ensure a given law is applied uniformly. Comitology refers to a set of procedures, including meetings of representative committees, that give Member States a say in the implementing acts. European Commission departments submit draft implementing acts to the responsible committees for an opinion.
2. Before each meeting, the Commission sends national authorities the invitation, agenda and draft implementing act. The comitology committee can then vote, either during the meeting or in writing afterwards. The European Commission publishes the voting results and the summary record of the meeting in the comitology register.

### 2.2.1.2. Expert Group

1. Expert groups assist and advise the Commission in various topics, in relation to the implementation of existing Union legislation, programmes and policies and the preparation of legislative proposals and policy initiatives while coordinating with Member States, exchange of views.
2. The expert group also provides expertise to the Commission when preparing implementing measures.

## 2.3. Actors

### 2.3.1. GROW

1. The Directorate-General for Internal Market, Industry, Entrepreneurship and SMEs (DG GROW) is the policy owner of Public Procurement in the EU and is in charge of the Public Procurement Directives.
2. Roles and tasks:
	1. Coordinates the eForms from the legal and policy side.
	2. Is the policy owner of eForms.
	3. Ensures coherence with the policy objectives.
	4. Organises committee meetings with the ACPC.
	5. Organises expert group meetings with EXPP and EXEP.
	6. Holds eForms workshops with the eForms Community.
	7. Provides support to policy makers, buyers and other users.
	8. Provides guides on eForms.
	9. Analyses and provides feedback on the issues received through Gitlab or other communication channels.
	10. Organises meetings and workshops: EXEP meetings, ACPC meetings, eForms Community workshops.

### 2.3.2. OP

1. The Publications Office of the EU (OP) has the responsibility to provide the technical specifications of eForms, which are called the Software Development Kit (SDK). OP is also responsible for Tenders Electronic Daily (TED), delivering data and services to reuse TED data, providing the eProcurement Ontology (ePO), managing EU Vocabularies, providing the data model on the European Single Procurement Document (ESPD) and providing the eNotices2 application to submit eForms notices via API or web interface.
2. Roles and tasks:
	1. Is in charge of the technical implementation of eForms.
	2. Provides the Software Development Kit (SDK).
	3. Runs Tenders Electronic Daily (TED).
	4. Provides the electronic Procurement Ontology (ePO).
	5. Provides eNotices2.
	6. Provides EU Vocabulary with all code lists.
	7. Manages the eSender Community.
	8. Provides support to users of eNotices2 and implementers.
	9. Support re-users of TED data.
	10. Ensures alignment with the ePO and the ESPD.
	11. Analyses and provides feedback on issues received through Gitlab, GitHub other communication channels.
	12. Participates in meetings and workshops: EXEP meetings, ACPC meetings, eForms Community workshops.

### 2.3.3. ACPC

1. The Advisory Committee on Public Contracts (ACPC) acts as the comitology body in the policy field of Public Procurement. This body is important for implementing EU legal acts including eForms.
2. Roles and tasks:
	1. Aids the adoption process of Implementing Regulations on eForms.
	2. Shares feedback from users and implementers to GROW and OP.
	3. Supports the improvement of the governance of eForms.
	4. Collaborates in strategic discussions on eForms.
	5. Votes on the proposed text and changes prior to the adoption of the amendment.
	6. Participates in meetings and workshops: ACPC meetings.

### 2.3.4. EXPP

1. The Commission Government Experts Group on Public Procurement (EXPP) is established to advises the Commission on its policy on public procurement. It is composed of government representatives from all EU Member States. Representatives come from a range of government bodies, including ministries of economy or finance, public procurement agencies, competition authorities, etc. The Group's task is to assist the Commission in the definition and development of the public procurement policy for the Internal Market, including its international dimension. It complements the Advisory Committee for Public Contracts, the comitology body in this policy field.
2. Tasks:
	1. Shares feedback from users and implementers to GROW.
	2. Supports discussions on eForms, which are of more legal nature and cannot be resolved by EXEP. They will be labelled `expp` in GitLab.
	3. Shares developments and best practices with national actors.
	4. Participates in meetings and workshops: eForms Community workshops.

### 2.3.5. EXEP

1. The Multi-stakeholder Expert Group on eProcurement (EXEP) assists and advises the Member States and the Commission in implementing the provisions of the new public procurement Directives relating to electronic procurement. It contributes to monitoring the uptake of eProcurement across the EU, sharing best practices and following new developments in the field, also addressing interoperability issues in this area.
2. Roles and tasks:
	1. Shares feedback from users and implementers to GROW and OP.
	2. Collaborates in strategic discussions on eForms and on digital procurement.
	3. Shares developments and best practices with national actors.
	4. Supports the improvement of the governance of eForms.
	5. Participates in meetings and workshops: EXEP meetings, eForms Community workshops.

### 2.3.6. eForms Community

1. The eForms Community is an active group of public procurement experts, comprising mainly actors coming from EXEP and EXPP.
2. Roles and tasks:
	1. Discusses and advises with GROW and OP on issues and topics around eForms.
	2. Provides input for the amendment procedure, including the needs from the national level.
	3. Discusses releases of eForms.
	4. Shares developments and best practices with national actors.

### 2.3.7. eForms GitLab Community 

1. The eForms GitLab Community is a group of public procurement experts, comprising mainly actors coming from public administration. It can also include actors from the eSenders community and data re-users.
2. Roles and tasks:
	1. Shares feedback from users and implementers.
	2. Submits issues related to eForms on GitLab and proposes solutions if possible.
	3. Supports discussions around eForms through GitLab.

### 2.3.8. eSenders

1. eSenders submit eForms notices electronically to TED.
2. Roles and tasks:
	1. Implements eForms in public and commercial eProcurement services.
	2. Are gateways from Member States to TED.
	3. Provides technical feedback to OP through GitHub.

### 2.3.9. Buyers

1. Buyers conduct public procurement procedures.
2. Roles and tasks:
	1. Are the main users of eForms by publishing notices.
	2. Are responsible for the data published on TED and on the national portals.
	3. Provide feedback on public procurement incl. eForms to policy makers.
	4. May provide feedback on GitLab.

### 2.3.10. Data re-users

1. Data re-users are consumers of TED data.
2. Roles and tasks:
	1. Provide commercial, research and other services around procurement notices.
	2. Provides technical feedback to OP through GitHub.
	3. May provide feedback on GitLab.

### 2.3.11. Other actors

1. There are also other actors around eForms that are shortly listed here:
	1. Tenderers: Suppliers participating in public procurement procedures.
	2. Public Procurement Excellence Hub: DGs of the European Commission.
	3. Competition authorities
	4. Citizens
	5. Business associations
	6. NGOs

# 3. Artefacts

## 3.1. eForms Implementing Regulation

1. The Implementing Regulation establishes the legal frame for the development and use of eForms. The aim is to adopt the eForms Regulation every second year through an amendment procedure. The amendment procedure can be triggered by request from Member States and the Commission to improve the overall functioning of eForms. The process of amending eForms can also be triggered through secondary legislation which has an impact on eForms which needs to be addressed. In this case the aim is to also address issues identified by Member States and the Commission to improve eForms. Together with the publication of a new Implementing Regulation, GROW provides a webpage for eForms [here](https://single-market-economy.ec.europa.eu/single-market/public-procurement/digital-procurement/eforms_en).

## 3.2. Annex

1. The Implementing Regulation on eForms includes also includes an Annex. The Annex describes in a matrix all fields and all forms that fall under the Implementing Regulation. A more accessible spreadsheet is available through a link from the GROW website on eForms [here](https://single-market-economy.ec.europa.eu/single-market/public-procurement/digital-procurement/eforms_en). During the implementation, after the adoption bugs or improvements are identified and solved, the updated Annex can be found [here](https://code.europa.eu/eproc/eforms/docs/-/tree/main/annex).

## 3.3. Software Development Toolkit (SDK)

1. The eForms SDK is a collection of resources providing the foundation for building eForms applications. It is provided and maintained by OP. The documentation on the SDK can be found [here](https://docs.ted.europa.eu/eforms/latest/). Information about the versioning (major, minor and revision) can be found [here](https://docs.ted.europa.eu/eforms-common/versioning/index.html).

## 3.3.1. Code lists in eForms

1. The Publications Office of the European Union (OP) has defined and published many vocabularies for use across a wide range of sectors on the [EU Vocabularies](https://op.europa.eu/en/web/eu-vocabularies/controlled-vocabularies) website. A subset of these vocabularies is relevant to the domain of eProcurement; most of these are flat lists of terms and their definitions; some, such as the NUTS (Nomenclature of Territorial Units for Statistics) are hierarchical taxonomies. They are listed on the [Authority tables and taxonomies used in eProcurement](https://op.europa.eu/en/web/eu-vocabularies/e-procurement/tables) page. More information on code lists and their use in eForms can be found [here](https://docs.ted.europa.eu/eforms/latest/codelists/index.html).

## 3.4. Guides

1. To support Member States transitioning from the old forms to the new forms an eForms Policy Implementation Handbook was published in 2020. It can be found [here](https://op.europa.eu/en/publication-detail/-/publication/73a78487-cc8b-11ea-adf7-01aa75ed71a1). Following the amendments and to better support buyers and policy makers, the EXEP and eForms Community requested more guidance. As a result, since 2024 guides are published for specific topics. They are intended for various actors and to support the understanding of especially new requirements introduced in eForms. They are drafted in collaboration with the Commission and the eForms Community. The guides explain for example how the Foreign Subsidies Regulation or the International Procurement Instrument works. All guides are provided as an online version and as PDFs, they can be found [here](https://code.europa.eu/eproc/eforms/docs/-/tree/main/guides).

# 4. Amendment procedure

1. The amendment procedure for eForms comprises several steps from the preparation to the final adoption. The following figure depicts the general phases.
2. The boxes in green show where actors, especially Member States, are involved in this process.  The yellow boxes depict the phases where an amendment enters into force, and the transition period. The durations of these can vary depending on the changes foreseen in the amendment.

![Figure on amendment procedures](images/eforms_amendment_procedure.png)

## 4.1. Preparation

1. The preparation of an amendment procedure is the most important phase when it comes to the content of an eForms amendment. It will start with an EXEP meeting to discuss as much as possible the changes foreseen in the amendment. After this, there will be regular eForms Community meetings for discussions but also meetings of the Public Procurement Excellence Hub to ensure that feedback from all DGs can be taken on board and discussed with the eForms Community workshops. The main objective is, that with the start of the inter service consultation, the draft of the amendment is very stable.
2. As the impact of changes will be clearer during the preparation phase, the European Commission will discuss the entry into force time and the transition period with the eForms Community, prior to the interservice consultation. The following two dates will be discussed:
	1. As of which date the eForms amendment will be applied. This is the date, when the transition period starts.
	2. The date from which the transition period ends.
3. The timeline will be included in the draft Implementing Regulation which will go through interservice consultation and public consultation.

### 4.1.1. Artefacts

#### 4.1.1.1. Draft amendment of eForms

1. The main artefact of the preparation will be the draft Implementing Regulation.

#### 4.1.1.2. Draft Annex

1. While the draft Implementing Regulation includes the Annex, the spreadsheet version is more accessible for review. It will contain the new Annex but also a version with tracked changes from the previous Implementing Regulation, to enable users to understand all the changes.

#### 4.1.1.3. Draft Guides

1. It can be expected that sectorial legislations have an impact on eForms. Especially for these, it is necessary to provide guides that are discussed with the respective DGs, the Commission and the eForms Community. This will help to get a mutual understanding of the impact and that they are correctly presented in the Implementing Regulation. The aim of the integration is also to reuse as much as possible existing fields to make it easier for buyers to provide the necessary information.

#### 4.1.1.4. Draft Code lists

1. For sectorial legislation, it might be necessary to create new code lists. A draft of the code lists will be provided during the preparation of the Implementing Regulation alongside the guides.

### 4.1.1.5. Translations

1. The translations of the annex are often used in the user interface to support the buyers. It is recommended that labels and descriptions are used consistently throughout the eProcurement service. The preparation phase should be used for two aspects.
	1. Translation corrections: It might be that the previous annex has aspects that a country would like to correct with the next amendment. In this case the country should provide a table with the corrections. If a language is used in more than one country, then those countries should provide the corrections together. The input will then be forwarded to the translation service of the Commission to use them with the next translation cycle. In all cases a contact point should be given, to allow the translator to get in contact if necessary.
	2. Translation improvements: The amendment procedure is also useful to improve the overall wording of the labels and especially the descriptions. During the preparation phase, the English version should be updated based on requests from users. While the meaning cannot change, a better description will certainly be welcomed by buyers.
2. During the preparation phase, Member States should provide the Commission with corrections of translations from the previous eForms amendment.

## 4.2. Interservice consultation

1. The internal interservice consultation is used for requesting and obtaining the formal opinion of other services of the European Commission with a legitimate interest in a draft text. GROW may implement the comments or object to them.
2. Due to the involvement of the Public Procurement Excellence Hub in the preparation phase, the aim is that the interservice consultation will not have any significant impact on the draft amendment.

## 4.3. Public consultation

1. GROW will launch a public consultation on the platform [Have Your Say](https://have-your-say.ec.europa.eu/index_en), inviting all interested parties to comment on the draft amendment. This platform is created for public consultation and feedback throughout which citizens and businesses can share their view on new EU policies and existing laws. At the beginning of the public consultation, GROW will organise a workshop with the eForms Community to discuss the outcome of the interservice consultation. This meeting will also be used to encourage the eForms Community to provide comments or share the public consultation with interested parties and other stakeholders.
2. At the end a second workshop with the eForms Community will be organised to discuss the outcome of the public consultation. After this, the draft amendment, code lists and guides are finalised. It will be more difficult after the consultation to introduce any changes without threatening the timeline.

## 4.4. Translations

1. GROW will launch the translations of the finalised amendment. If contact points for the translators are available, this could support translations during the process. Once the translated versions are available, they will be sent to the EXEP for review.

## 4.5. Comitology

1. The ACPC will review the amendment. A Committee meeting (indicative voting) will take place. The draft text will then be sent for vote through written procedure. The Commission will then decide on the amendment, taking into consideration the vote delivered.

## 4.6. Adoption and entry into force

1. If the voting was positive, the outcome will be given to the Cabinet for approval through a written procedure. After the approval, the amendment will be adopted and published on the Official Journal, and also on the ‘[Have Your Say](https://ec.europa.eu/info/law/better-regulation/have-your-say_en)’ platform (translated in all languages) and transmitted to the co-legislators. Member States can then share the amendment on the national level.

## 4.7. Application and end of transition

1. With the application of the amendment the Publications Office will provide a new SDK. This SDK is in line with the amendment and provides all the required fields and changes. There is a transition period foreseen for Member States to switch to the new SDK, in order to provide smooth transition. The transition period starts with the application of the amendment.
2. The end of the transition period marks the official date when the new amendment becomes mandatory.

# 5. Intermediate releases

1. Besides releases related to the amendment procedures (see section 4), it can be safely assumed that further releases will be provided to overall improve the functioning of eForms between amendments. The general principle is that there will be only one SDK release every 6 months for business needs. This may change due to the urgency of the release or for technical reasons.
2. The following figure shows the release cycle for intermediate releases. The boxes in green show where actors, especially Member States, are involved in this process. The yellow box depicts as of when the SDK is available for implementation.

![Figure on intermediate releases](images/eforms_intermediate_releases.png)

## 5.1. Collection

1. At any time, it is possible for users to create issues on GitLab. The Commission will assess them and provide feedback. For bugfixes, it will not be necessary to have discussions, they will be implemented for the next release.
2. Depending on the intermediate release, there will be different kinds of artefacts:
	1. Improved Annex: Depending on the issue, it might have an impact on the Annex. The improved Annex will contain a clean updated version but also a version with tracked changes from the previous Implementing Regulation, to enable users to understand all the changes.
	2. Updated code lists: Depending on the issue, it might be necessary to update the code lists. They will be prepared for discussion with the eForms Community.
	3. Updated or new guides: Depending on the issue, it can also have an impact on an existing guide. It may also lead to the writing of new guides. 

## 5.2. Discussion

1. The meetings with the eForms Community, which will usually take place every second month, will be used to discuss open issues and prepared artefacts.

## 5.2. Release

1. Once the issues are discussed and agreed, a new version of the SDK will be provided.

# 6. Risk management

1. Risk management in software involves identifying, assessing, and mitigating potential issues that could impact project success, ensuring timely delivery and quality.
2. The following table shows potential risks and how they can be addressed.

| **Risk**                                            | **Causes**                                                                                         | **Effects**                                                                                                         | **Response Strategy** | **Risk/Threat Response**                                                                                                                                                                                                                                           |
| --------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------ | ------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Unclear fields usage**                            | - concise Regulation Annex                                                                         | - difficulty to tailor for MSs  <br>- inappropriate technical implementation  <br>- reduced data quality            | Mitigation            | Preventive actions:  <br>- provide additional information in the form of guides developed in collaboration with technical and business people.                                                                                                                     |
| **Not technically implementable rule and/or field** | - not consideration of technical limitations                                                       | - not implemented requirements                                                                                      | Mitigation            | Preventive actions:  <br>- involve technical people  <br>- have a mock mark-up                                                                                                                                                                                     |
| **SDK delay**                                       | - SDK scope creep  <br>- other related projects delay                                              | - shorter time for eSenders to develop and transition on time                                                       | Mitigation            | Preventive action:  <br>- reject unnecessary SDK scope changes (reduce probability)  <br>Contingency response:  <br>- extend usability period of previous SDKs (reduce impact)  <br>- availability of using eNotices2 for cases where new fields would be required |
| **New sectorial legislation impacting eForms**      | - Short term adoption of an unforeseen legislation impacting publications about Public Procurement | - need to update eForms                                                                                             | Mitigation            | Contingency response:  <br>- align schedule with the one of eForms amendment whenever possible  <br>- Evaluate the urgency aspect, identify the SDK version it should go with and anticipate the next amendment                                                    |
| **Administrative burden**                           | New fields and SDKs introduced                                                                     | - the implementation of the new SDK along with the extension of eForms could pose additional administrative burdens | Mitigation            | Preventive action:<br>- Reuse existing fields<br>- Provide guides                                                                                                                                                                                                  |

Table: List of risks and how to respond to them